const express = require('express');
// Mongoose is a package that allows creation of schemas to model our data structures and to have an access to a number of methods for manipulating our database.

const mongoose = require("mongoose");

const app = express();
const port = 3001

// MongoDB Connection
// Connecting to MongoDb Atlas
mongoose.connect("mongodb+srv://Shiela1028:JesusChrist777@wdc028-course-booking.tflhi.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	// prevents future errors
	useNewurlParser: true,
	useUnifiedTopology: true
})
// ERROR HANDLING
// Set notifications for connection success or failure
let db = mongoose.connection;
// if error occured, output in the consoles
db.on("error" , console.error.bind(console,"connection error"))
// if the connection is successful, output in the console
db.once('open', () => console.log("We're connected to the cloud database."))

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// Mongoose Schemas
// Blueprint or template
// Schemas determine the structure of the docs to be written in the database
// We will use Schema() constructor of the Mongoose module to create a new schema object
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value
		default:"pending"
	}
})
const Task = mongoose.model("Task", taskSchema)
// variable must be singular and starts with capital letter following the MVC approach for naming conventions
// 1st para = indicates the collection in where to store the data
// 2nd para = used to specify the schema/blueprint variable

// MODELS are what allows us to gain access to methods that will perform CRUD functions.
// 

// Routes

// Create a new task
/*
Business Logic
1. Add a functionality to check if there are duplicate tasks
	-if the task already exists, return there is a duplicate
	-if the task doesn't exist, we can add it in the database
2. The task data will be coming from the request body
3. create new tASK OBJECT with properties that we need
4. save the data
*/

app.post("/tasks",(req,res) =>{
	// Check if there are duplicate tasks
	// findOne() is a mongoose method that acts similar to "find"
	// it returns the first document that matches the search criteria
	// ERROR HANDLING
	Task.findOne({name: req.body.name}, (error, result)=>{
		// if a document was found and matches the info sent via client/postman
		if(result !== null && result.name == req.body.name){
			// return a message to the client/postman
			return res.send("Duplicate task found")
		}else{
			// if no document was found
			// Create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			})
			newTask.save((saveErr, savedTask)=> {
				// if there are errors in saving
				if(saveErr){
					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					//Errors normally come as an object data type
					return console.error(saveErr)
				}else{
					// no error found while creating the document
					// Return a status code of 201 for successful creation
					return res.status(201).send("New Task Created")
				}
			})
		}	
	})
})

/*
Retrieving all data
Business Logic
1. Retrieve all the documents using the find()
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client and return an array of documents
*/

app.get('/tasks', (req,res)=>{
	//return all docs and stores in the result para of the call back function. empty {}
	Task.find( {}, (err,result)=>{
		// if an error occurred
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				result
			}) //converts docs to json format
		}
	} ) 
})

/*
Register a user(Business Logic) ACTIVITY
1. Find if there are duplicate user
	-if user already exists, we return an error
	-if user doesnt exist, we add it in the database
 		-if the username and pass are both not blank
			if blank, send response "BOTH username and password must be provided"
			-both condition has been met, create a new object.
			-save the new object
				-if error,return an eror message
				-else, response a status 201 and "New User Registered"
*/

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})
const User = mongoose.model("User", userSchema)
const Pass = mongoose.model("Pass", userSchema)
app.post("/Signup",(req,res) =>{
	// ERROR HANDLING
	// find docs with matching username
	User.findOne({username: req.body.username}, (error, result)=>{	
		// check for duplicates
		if(result !== null && result.username === req.body.username){
			// return a message to the client/postman
			return res.send("Duplicate Users found")
			/*}else if(result === ({}) && result.username == null && result.password == null){
				return res.send("BOTH username and password must be PROVIDED!")*/
			}else{
				// No duplicates found
				if(req.body.username !== "" && req.body.password !== ""){
					// create a new user
					// let newUser = new User({
						// username: req.body.username	
						// password: req.body.password
					// })
					let newUser = new User({
					username: req.body.username	
					})	
					let newPass = new Pass({
					password: req.body.password	
					})	

					newUser.save((saveErr, savedUser)=> {
				if(saveErr){			
					return console.error(saveErr)
				}else{			
					return res.status(201).send("New Users registered")
				}
			})

				}else{
					// if the username or pass was left blank
					return res.send("BOTH username and password must be PROVIDED!")
				}
			
				
		}	
	})
})

app.get('/users', (req,res)=>{
	//return all docs and stores in the result para of the call back function. empty {}
	User.find( {}, (err,result)=>{
		// if an error occurred
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				result
			}) //converts docs to json format
		}
	} ) 
})

// SOLUTION


app.listen(port, ()=> console.log(`Server is running at port ${port}`))

